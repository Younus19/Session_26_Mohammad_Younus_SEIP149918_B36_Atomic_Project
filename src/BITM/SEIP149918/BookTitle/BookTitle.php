<?php
namespace App\BookTitle;
use App\Model\Database as DB;
use App\Utility\Utility;
use App\Message\Message;

class BookTitle extends DB{
    public $id;
    public $book_title;
    public $author_name;
    public function __construct(){

        parent::__construct();
    }


    public function setData($data=NULL){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('book_title',$data)){
            $this->book_title=$data['book_title'];
        }
        if(array_key_exists('author_name',$data)){
            $this->author_name=$data['author_name'];
        }
    }
    public function store(){
        $arrData = array($this->book_title,$this->author_name);
        $sql="insert into book_title(book_title, author_name) VALUES (?,?)";

        $STH= $this->DBH->prepare($sql); //create a object
        $result= $STH->execute($arrData);
        if($result)
            Message::setMessage("Success! Data has been inserted successfully!");
        else
            Message::setMessage("Fail! Data has not been inserted successfully");

        Utility::redirect('create.php');

    }
    public function index($fetchMode='ASSOC'){

        $STH = $this->DBH->query('SELECT * from book_title' );

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(\PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(\PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();

    public function view($fetchMode='ASSOC'){
        $sql= 'SELECT * from book_title WHERE id ='.$this->id;

        $STH = $this->DBH->query($sql );

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(\PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(\PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;


    }// end of index();




}

