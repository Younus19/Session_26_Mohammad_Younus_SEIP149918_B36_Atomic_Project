<?php
namespace App\Gender;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;


class Gender extends DB
{
    public $id;
    public $name;
    public $gender;

    public function __construct(){

        parent::__construct();
    }
    public function index(){
        echo "I'm inside the index of Gender Class";
    }
    public function setData($data=NULL){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('name',$data)){
            $this->name=$data['name'];
        }
        if(array_key_exists('gender',$data)){
            $this->gender=$data['gender'];
        }
    }
    public function store(){
        $arrData = array($this->name,$this->gender);
        $sql="insert into gender(name, gender) VALUES (?,?)";

        $STH= $this->DBH->prepare($sql); //create a object
        $result= $STH->execute($arrData);
        if($result)
            Message::setMessage("Success! Data has been inserted successfully!");
        else
            Message::setMessage("Fail! Data has not been inserted successfully");

        Utility::redirect('create.php');

    }


}
