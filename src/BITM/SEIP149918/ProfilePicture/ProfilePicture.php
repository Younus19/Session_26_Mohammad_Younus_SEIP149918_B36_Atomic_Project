<?php
namespace App\ProfilePicture;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;

class ProfilePicture extends DB
{
    public $id;
    public $name;
    public $picture;


    public function __construct()
    {

        parent::__construct();
    }

    public function index()
    {
        echo "I'm inside the index of ProfilePicture Class";
    }

    public function setData($data = NULL)
    {
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('name', $data)) {
            $this->name = $data['name'];
        }


    }
    public function setImageData($data=NULL)
    {
        if (array_key_exists('picture', $data)) {
            $this->picture = $data['picture']['name'];
        }
    }

    public function store()
    {
        $path='C:\xampp\htdocs\Lab_Exam7_Mohammad_Younus_SEIP149918_B36_Atomic_Project\src\BITM\SEIP149918\ProfilePicture\Upload\upload';
        $uploadedFile = $path.basename($this->picture);
        move_uploaded_file($_FILES['picture']['tmp_name'], $uploadedFile);


        $arrData = array($this->name, $this->picture);

       $sql = "insert into profile_picture(name, picture) VALUES (?,?)";

       $STH = $this->DBH->prepare($sql); //create a object
       $result = $STH->execute($arrData);
        if ($result)
            Message::setMessage("Success! Data has been inserted successfully!");
        else
            Message::setMessage("Fail! Data has not been inserted successfully");

        Utility::redirect('create.php');

    }

}
