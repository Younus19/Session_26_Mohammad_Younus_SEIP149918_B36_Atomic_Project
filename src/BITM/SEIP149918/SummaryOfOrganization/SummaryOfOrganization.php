<?php
namespace App\SummaryOfOrganization;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;

class SummaryOfOrganization extends DB
{
    public $id;
    public $organization_name;
    public $summary;

    public function __construct(){

        parent::__construct();
    }
    public function index(){
        echo "I'm inside the index of SummaryOfOrganization Class";
    }
    public function setData($data=NULL){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('organization_name',$data)){
            $this->organization_name=$data['organization_name'];
        }
        if(array_key_exists('summary',$data)){
            $this->summary=$data['summary'];
        }
    }
    public function store(){
        $arrData = array($this->organization_name,$this->summary);
        $sql="insert into summary_of_organization(organization_name, summary) VALUES (?,?)";

        $STH= $this->DBH->prepare($sql); //create a object
        $result= $STH->execute($arrData);
        if($result)
            Message::setMessage("Success! Data has been inserted successfully!");
        else
            Message::setMessage("Fail! Data has not been inserted successfully");

        Utility::redirect('create.php');

    }
}